/**/
const CALCTIME = true;
/*






*/
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
//
//														Interpolation of Lines
//
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
///////////////////////////////////////////////////////////////////////////////////////

export function interpolateLines(linesIn, scalar, width){


	// ///   TIME FOR EXECUTION   ------ ------ ------ ------ ------
	if (CALCTIME){var CALCTIME_interpolateLines = performance.now()}
	// ///   ---- --- ---------   ------ ------ ------ ------ ------



	/**  	Line Array  =  [ Array([ x1, y1, x2, y2 ]), ...  ]   */

	let points = {
			blue:{ x:[], y:[] },
			red: { x:[], y:[] },
	}

	///// - ---   Scale
	const scaleLine = (reciprocal=>lineNums=>lineNums.map(n=>reciprocal*n))(1/scalar)
	///// - ---


	function boxAndFill(colorField,ln){

		//	Isolated Storage
		const outlineCoords = { x:[], y:[] }

		let [ x1, y1,  x2, y2 ]
			= scaleLine(
			[ linesIn[colorField][ln][0], linesIn[colorField][ln][1],
				linesIn[colorField][ln][2], linesIn[colorField][ln][3] ])

		let pointsInLine = Math.floor(Math.sqrt( Math.pow(x1 - x2,2) + Math.pow(y1 - y2,2)))


		// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=//
		//     https://math.stackexchange.com/questions/2043054/find-a-point-on-a-perpendicular-line-a-given-distance-from-another-point    //
		// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=// //=//


		//	Find Four Corners
		let m = ((y2-y1)/(x2-x1))
		let distance = width > 0 ? width : 0


		//	If Slope is '0'
		if ( m === 0 ){

			//	Four Corner Coordinates
			var [ xuA, xuB, xlC, xlD ] = [ x1, x1, x2, x2 ]
			var [ yuA, yuB, ylC, ylD ] = [ y1 + distance, y1 - distance, y2 + distance, y2 - distance ]

			//	Add the Four Corners
			outlineCoords.x.push(...[ xuA, xuB, xlC, xlD ])
			outlineCoords.y.push(...[ yuA, yuB, ylC, ylD ])

		} else {

			//	Part of the Equation to find the Perpendicular line
			let plusOrMinus = ((bottom=(1+(1/(m**2)))) => d => Math.sqrt((d**2)/bottom))()(distance)

			let findXs = xS => [ (xS + plusOrMinus), (xS - plusOrMinus) ]
			let findYs = ((oppositeReciprocalSlope=(-1/m))=>
				(xP,yP)=>(xS_A,xS_B)=>
					[ oppositeReciprocalSlope*(xS_A-xP)+yP, oppositeReciprocalSlope*(xS_B-xP)+yP ])()

			//	Four Corner Coordinates
			var [ xuA, xuB, xlC, xlD ] = [ ...findXs(x1), ...findXs(x2) ]
			var [ yuA, yuB, ylC, ylD ] = [ ...findYs(x1,y1)(xuA,xuB), ...findYs(x2,y2)(xlC,xlD) ]

			let roundedPoints = [ [xuA,xuB,xlC,xlD], [yuA,yuB,ylC,ylD] ].map(arr=>arr.map(n=>Math.round(n)))

			//	Add the Four Corners
			outlineCoords.x.push(...roundedPoints[0])
			outlineCoords.y.push(...roundedPoints[1])
		}


		/*        Points along the line        */
		/*      A + (B - A) * stepPercent      */

		// /// // /// // /// // /// // /// // /// // /// // ///

		//	Interpolated Points  -  Outer Lines
		let lineLeftOffset =  { x:(xlC - xuA), y:(ylC - yuA) }
		let lineRightOffset = { x:(xlD - xuB), y:(ylD - yuB) }

		for(let i=1; i<pointsInLine; ++i) {
			let stepPercent = i / pointsInLine

			outlineCoords.x.push(...[(xuA + lineLeftOffset.x * stepPercent), (xuB + lineRightOffset.x * stepPercent)].map(n => Math.round(n)))
			outlineCoords.y.push(...[(yuA + lineLeftOffset.y * stepPercent), (yuB + lineRightOffset.y * stepPercent)].map(n => Math.round(n)))
		}

		// /// // /// // /// // /// // /// // /// // /// // ///

		//	Interpolated Points  -  Top/Bottom Lines
		let lineTopOffset =    { x:(xuB - xuA), y:(yuB - yuA) }
		let lineBottomOffset = { x:(xlD - xlC), y:(ylD - ylC) }

		let thickness = (distance*2)
		for(let i=1; i<thickness; ++i) {
			let stepPercent = i / thickness

			outlineCoords.x.push(...[(xuA + lineTopOffset.x * stepPercent), (xlC + lineBottomOffset.x * stepPercent)].map(n => Math.round(n)))
			outlineCoords.y.push(...[(yuA + lineTopOffset.y * stepPercent), (ylC + lineBottomOffset.y * stepPercent)].map(n => Math.round(n)))
		}


		/*     ===   LAST   ===     */
		/* *-* *-* *-* *-* *-* *-* *-* *-* *-* *-* */

		//	Fill the line in
		fillBox( outlineCoords )

		points[colorField].x.push(...outlineCoords.x)
		points[colorField].y.push(...outlineCoords.y)
		/* *-* *-* *-* *-* *-* *-* *-* *-* *-* *-* */


	}


	//	Fill Lines
	if(linesIn.blue) { linesIn.blue.forEach( (lD,ln)=>boxAndFill('blue', ln)) }
	if(linesIn.red)  { linesIn.red.forEach(  (lD,ln)=>boxAndFill('red',  ln)) }


	//	Finish with only unique points
	Object.values(points).forEach((color,i)=>outputFormat( color, removeDuplicatePoints( color )))


	// ///   TIME FOR EXECUTION   ------ ------ ------ ------ ------ ------ ------ ------ ------ ------ ------ ------
	if (CALCTIME){console.log('CALCTIME_interpolateLines :: ',(performance.now() - CALCTIME_interpolateLines),'ms')}
	// ///   ---- --- ---------   ------ ------ ------ ------ ------ ------ ------ ------ ------ ------ ------ ------


	return points
}

///////////////////////////////////////////////////////////////////////////////////////
/*






*/
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
//
//														Remove Duplicate Points
//
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
///////////////////////////////////////////////////////////////////////////////////////

function removeDuplicatePoints(pointsPresent){

	//	Arranged Array of Points   =>   [ [x,y], [x,y], ... ]
	return Array.from(new Set(((ps=Object.values(pointsPresent))=>
		ps[0].map((xS,i)=>JSON.stringify([xS,ps[1][i]])))()))
	.map(uniquePair=>JSON.parse(uniquePair))

}

///////////////////////////////////////////////////////////////////////////////////////
/*






*/
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
//
//														Convert to Output Format
//
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
///////////////////////////////////////////////////////////////////////////////////////

function outputFormat(pointData,uniquePoints){

	//	Number of Unique Points
	let unqSize = uniquePoints.length

	//	Hold New Points
	let newX = new Array(unqSize)
	let newY = new Array(unqSize)

	//	Refill Points
	uniquePoints.forEach(upp=>{
		--unqSize
		newX[unqSize] = upp[0]
		newY[unqSize] = upp[1]
	})

	//	Replace the arrays in the given set of points
	pointData.x = newX
	pointData.y = newY

	return unqSize
}

///////////////////////////////////////////////////////////////////////////////////////
/*






*/
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
//
//													         Fill in Box
//
/* *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* = *-* */
/*

*/
/*						Find next Scan Line



		if the Y value is not equal to the Y value before it,
		this is the start of a new line.


		Ex)
				[4555,199]
				[5555,199]	<-	End of Line

				[4555,200]	<-	Start of Line
				[5555,200]


*/
/*						Find gap marking points in Scan Line



		find point on the left side of the gap (last)

		find point on the right side of the gap (first)

		fill between those

		go to next line


		Ex)
				[21,5555]
				[22,5555]

				[23,5555]	<-	Left side of gap
				[57,5555]	<-	Right side of gap

				[58,5555]
				[59,5555]


		The points will be next to each other,
		with a difference greater than 1 between their X values.


*/
///////////////////////////////////////////////////////////////////////////////////////

function fillBox( outlineData ){

	//	Clean Up Line Data  -  And Sort Ascending
	let uniquePoints = removeDuplicatePoints(outlineData)
	//	Sort 'Y' ascending, and by 'X' ascending if necessary
	.sort(([a, b], [c, d]) =>  b - d || a - c )
	/* ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- */


	//	Highest and Lowest points  =>  y
	let miny = uniquePoints[0][1]
	let maxy = uniquePoints[uniquePoints.length-1][1]

	//	Number of lines in-between
	let scanLines = (maxy - miny) - 1

	/*
				1.	Find Next Line
				2.	Find Left & Right Gap Points
				3.	Fill in that gap
	*/

	let currentIndex = 0
	for (let sl=0; sl<scanLines; ++sl) {

		//	Gap
		let hasGap = true

		//	Find Next Line
		let lineStartIndex = currentIndex
		while( uniquePoints[++lineStartIndex][1] === uniquePoints[currentIndex][1] ){}

		//	Find Gap Start & Stop Point
		let gapStopIndex = lineStartIndex
		while( hasGap && (uniquePoints[gapStopIndex][0] - uniquePoints[++gapStopIndex][0]) >= -1 ){

			// if went to next line  =>  notify & stop
			if ( uniquePoints[gapStopIndex][1] > uniquePoints[lineStartIndex][1] ){
				hasGap = false;
				currentIndex = gapStopIndex - 1
			}
		}


		//	Fill Gap
		if (hasGap) {

			let gapY = uniquePoints[gapStopIndex][1]
			let gapStartX = uniquePoints[gapStopIndex-1][0]
			let gapSize = uniquePoints[gapStopIndex][0] - gapStartX

			let filledGap = new Array( gapSize - 1)
			for (let bridge=1; bridge<gapSize; ++bridge){
				filledGap[bridge-1] = [ (gapStartX + bridge), gapY ]
			}

			// End  --- --- --- --- --- ---

			//	Append the new points
			uniquePoints.push(...filledGap)

			//	Ready next starting point
			currentIndex = gapStopIndex
		}
	}


	/* ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- ~-~- */
	//	RETURN  -  LAST
	outputFormat(outlineData,uniquePoints)

}

///////////////////////////////////////////////////////////////////////////////////////




