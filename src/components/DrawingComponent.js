import React, { useState, useEffect } from 'react'
import { Stage, Layer, Rect, Circle, Line, Image as Kimage } from 'react-konva';
import Konva from 'konva';

import * as ContourLogic from './helpers/ContourLogic'


//--------------------------------------------------
//		 https://github.com/konvajs/react-konva
//--------------------------------------------------



//:::::::::::::::::::::::::::::::
//	 Drawing Component Example
//:::::::::::::::::::::::::::::::
export default () => {

	const keyframeImageSrc = 'https://cdn.eso.org/images/thumb300y/eso1907a.jpg'
	const [lines,setLines] = useState({ lines:{}, properties:{ scale:0.5, thickness:0 } })
	const [points,setPoints] = useState({})	// blue, red == x:[], y:[]

	useEffect(()=>{
		setPoints(ContourLogic.interpolateLines(lines.lines, lines.properties.scale, lines.properties.thickness))
	},[lines.lines])

	return(
		<>
			<DrawingTablet setLines={setLines}  keyframeImageSrc={keyframeImageSrc}/>
			{/*<DisplayPixels points={points}  keyframeImageSrc={keyframeImageSrc}/>*/}
		</>
	)
}/*





*/
//:::::::::::::::::::::
//		Drawing Zone
//:::::::::::::::::::::

let myMouseLocation = { last: [-100,-100], current: [-99,-99] }

function DrawingTablet({setLines, keyframeImageSrc}) {

	//	TODO:		let stage = {}  	// To Use Methods


	//	Color Information
	/**
	 * 		'True'  = BLUE
	 * 		'False' = RED
	 */
	const [lineColor,setLineColor] = useState(true)

	//	Line Information
	const [lineData,setLineData] = useState({ busy:false, lines:{blue:[],red:[]}, buffer:[], scale:2, thickness:1 })
	///   ^  Set 'Scale' Here		//	TODO:  'scale' = 1 , 'thickness' = 'pixels'


	/*    Handles Clicking On Canvas    */
	const handleDraw = e => {
		if (!lineData.busy){
			setLineData({ ...lineData, busy: true, buffer:[e.evt.layerX, e.evt.layerY]})
		}
		else {
			let categorizeLineByColor = c => {return{ ...lineData.lines, [c]:lineData.lines[c].concat([lineData.buffer.concat([e.evt.layerX, e.evt.layerY])])}}
			setLineData({
				busy: false,
				lines: lineColor ? categorizeLineByColor('blue') : categorizeLineByColor('red'),
				buffer:[],
				scale: lineData.scale,
				thickness : lineData.thickness
			})
		}
		setMouseLocation( [e.evt.layerX, e.evt.layerY] )
	}
	/*  - -  - -  - -  - -  - -  - -  - -  */



	/*    Image    */

	let imageToContour = new Image();
	imageToContour.src = keyframeImageSrc


	const [keyframeImage, imageLoaded] = useState(false)

	useEffect(()=>{
		imageToContour.width  = imageToContour.width  * lineData.scale
		imageToContour.height = imageToContour.height * lineData.scale
		imageLoaded(imageToContour)
	},[imageToContour.complete])


	const drawSurfaceSize = keyframeImage ? { width: keyframeImage.width, height: keyframeImage.height }
	: { width: 10, height: 10 }

	/* ~ ~ ~ ~ ~ ~ ~ */


	const [mouseLocation, setMouseLocation] = useState([-100,-100])

	const handleFollowCursor = e => {
		myMouseLocation.current = [ e.evt.layerX, e.evt.layerY ]
	}

	// Track Mouse Movement
	function loop() {
		window.requestAnimationFrame(() => {
			if ( (myMouseLocation.last[0] !== myMouseLocation.current[0]) && (myMouseLocation.last[1] !== myMouseLocation.current[1]) ){
				setMouseLocation( myMouseLocation.current )
				myMouseLocation.last = myMouseLocation.current
			}
			loop()  // 60 fps (monitor refresh rate)
		});
	}
	//	Begin loop
	useEffect(()=>{
		loop();
	},[])





	return(
		<>

			{/*   Send Drawing Data   */}
			<button onClick={e => {  //console.log(lineData.lines);  console.log(stage)
				//e.preventDefault()
				setLines({ lines: lineData.lines, properties:{ scale: lineData.scale, thickness: lineData.thickness } })
			}}>Send Drawing Data</button>


			{/*   Select Color   */}
			<button onClick={() => {setLineColor(!lineColor)}}>Switch Color</button>
			<div style={{width:'5em',border: `2px solid ${lineColor ? 'blue' : 'red'}`, display: 'inline-block', textAlign:'center'}}>{lineColor ? 'BLUE' : 'RED'}</div>


			{/*   Scale   */}
			<div style={{width:'5em',border: '2px solid green', display: 'inline-block', textAlign:'center'}}>Scale: {lineData.scale}</div>  {/* TESTING */}


			{/*   Drawing Tablet Space   */}
			<div style={{
				maxWidth:  `${drawSurfaceSize.width}px`,
				maxHeight: `${drawSurfaceSize.height}px` }}>
			<Stage
				//	TODO:		ref={r => (stage = r)}

				width={ drawSurfaceSize.width }
				height={ drawSurfaceSize.height }
				//style={{border: '5px solid rgb(155, 158, 118)'}}

				onMouseDown={handleDraw}
				onMouseMove={handleFollowCursor}
			>
				<Layer>

				{/*   IMAGE  BACKGROUND   */}
					<Kimage image={ keyframeImage }/>

				{/*    DRAWING  SURFACE    */}
					{/*   Draw Your Lines   */}
					{/*   Blue   -   Red    */}
					{Object.values(lineData.lines).map((p,i)=>
						p.map((pc,k)=>
									<Line
									key={k}
									points={pc}
									stroke={i ? 'red' : 'blue'}
									strokeWidth={5} //1.25
									transformsEnabled ='position'
									/>)
					)}

					{/*   "Start Line" Notification Circle   */}
					{lineData.busy ? (
						<Circle
							x={lineData.buffer[0]}
							y={lineData.buffer[1]}
							radius={8}
							fill={'limegreen'}
							stroke={'black'}
							strokeWidth={2}
							transformsEnabled ='position'
						/>
					): null}

					<Circle x={mouseLocation[0]} y={mouseLocation[1]} radius={4} fill='pink' />

				</Layer>
			</Stage>
			</div>
		</>
	)
}/*





*/
//:::::::::::::::::::::::::::::::::::::::::::
//		Display Pixels Interpolated Drawing      -     [!]  ONLY USED FOR TESTING  [!]
//:::::::::::::::::::::::::::::::::::::::::::



function DisplayPixels({points, keyframeImageSrc}){


	/*    Image    */

	let imageToContour = new Image();
	imageToContour.src = keyframeImageSrc


	const [keyframeImage, imageLoaded] = useState(false)

	useEffect(()=>{
		//console.log(imageToContour.complete)
		imageLoaded(imageToContour)
	},[imageToContour.complete])


	const drawSurfaceSize = keyframeImage ? { width: keyframeImage.width, height: keyframeImage.height }
		: { width: 10, height: 10 }

	/* ~ ~ ~ ~ ~ ~ ~ */


	const [drawingToImage, setDrawingToImage] = useState(null)

	function testImage(){

		let myImageLayer = new Konva.Layer()

		let pixels = Object.values(points).map((p,i)=>
			p.x.map((px,k)=> {
				let newPixel = new Konva.Rect({
					x: px,
					y: p.y[k],
					width: 1,
					height: 1,
					fill: i ? 'red' : 'blue'
				})
				return(newPixel)
			})
		)
		myImageLayer.add(...[...pixels[0],...pixels[1]])
		myImageLayer.toImage({
			callback(img){setDrawingToImage(img)},
			width: keyframeImage.width,
			height: keyframeImage.height
		})

	}




	/*___________________________________________________________________*/

	return (
		<>
			<button onClick={() => {console.log(points)}}>
				Console Log Sent Drawing Data
			</button>
			<button onClick={testImage}>
				testImage
			</button>
			<Stage
				width={drawSurfaceSize.width}
				height={drawSurfaceSize.height}
				//style={{border: '5px solid rgb(155, 128, 118)'}}
			>

				{/*   IMAGE  LAYER   */}
				<Layer>
					{/*<Kimage image={ keyframeImage }/>*/}
					<Kimage image={drawingToImage} />
				</Layer>

				{/*<Layer>*/}
				{/*	{Object.values(points).map((p,i)=>*/}
				{/*			p.x.map((px,k)=> {*/}
				{/*				return(*/}
				{/*				<Rect*/}
				{/*					key={k}*/}
				{/*					x={px}*/}
				{/*					y={p.y[k]}*/}
				{/*					width={1}*/}
				{/*					height={1}*/}
				{/*					//fill={i ? 'red' : 'blue'}*/}
				{/*					fill={i ? i===1 ? 'red' : 'green' : 'blue'}*/}
				{/*				/>*/}
				{/*				)})*/}
				{/*		)}*/}
				{/*</Layer>*/}

			</Stage>
		</>
	)
}








